package com.example.appmicalculadora

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {

    private lateinit var txtUsuario: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var txtResultado: TextView

    private lateinit var btnSuma: Button
    private lateinit var btnResta: Button
    private lateinit var btnMulti: Button
    private lateinit var btnDivision: Button

    private lateinit var btnSalir: Button
    private lateinit var btnLimpiar: Button
    private lateinit var operaciones: Operacion

    var opcion: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventoClic()
    }

    private fun iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById(R.id.txtResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)

        btnSalir = findViewById(R.id.btnSalir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnDivision = findViewById(R.id.btnDivision)
        btnMulti = findViewById(R.id.btnMulti)
        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)

        val bundle: Bundle? = intent.extras
        txtUsuario.text = bundle?.getString("usuario")
    }

    private fun validar(): Boolean {
        return !(txtNum1.text.toString().isEmpty() || txtNum2.text.toString().isEmpty())
    }

    private fun realizarOperacion(): Float {
        val num1: Float
        val num2: Float
        var res: Float = 0f

        if (validar()) {
            num1 = txtNum1.text.toString().toFloat()
            num2 = txtNum2.text.toString().toFloat()
            operaciones = Operacion(num1, num2)

            res = when (opcion) {
                1 -> operaciones.sumar()
                2 -> operaciones.resta()
                3 -> operaciones.multi()
                4 -> operaciones.division()
                else -> 0f
            }
        } else {
            Toast.makeText(this, "Error: Faltaron datos", Toast.LENGTH_SHORT).show()
        }
        return res
    }

    private fun eventoClic() {
        btnSuma.setOnClickListener {
            opcion = 1
            txtResultado.text = realizarOperacion().toString()
        }
        btnResta.setOnClickListener {
            opcion = 2
            txtResultado.text = realizarOperacion().toString()
        }
        btnMulti.setOnClickListener {
            opcion = 3
            txtResultado.text = realizarOperacion().toString()
        }
        btnDivision.setOnClickListener {
            if (txtNum2.text.toString().toFloat() == 0f) {
                txtResultado.text = "No es posible dividir entre 0"
            } else {
                opcion = 4
                txtResultado.text = realizarOperacion().toString()
            }
        }
        btnSalir.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿Desea salir?")

            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                this.finish()
            }
            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                // No hacer nada
            }
            builder.show()
        }
        btnLimpiar.setOnClickListener {
            txtNum1.text.clear()
            txtNum2.text.clear()
            txtResultado.text = "Resultado : "
        }
    }
}